A demo service that analyzes GPX logs.

Run via the IDE or via `./gradlew run`. Then:

```
curl -v -F 'file=@path/to/gpxfile' http://localhost:9080/analyze-gpx
```
