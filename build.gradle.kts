import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.50"
    id("application")
    id("com.github.ben-manes.versions") version "0.25.0"
}

val deps by extra {
    mapOf(
            "ktor" to "1.2.4",
            "jackson" to "2.9.9",
            "slf4j" to "1.7.26",
            "junit" to "5.5.2"
    )
}

application {
    mainClassName = "org.mpierce.activitydemo.DemoMainKt"
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.ktor:ktor-server-core:${deps["ktor"]}")
    implementation("io.ktor:ktor-server-netty:${deps["ktor"]}")
    implementation("io.ktor:ktor-jackson:${deps["ktor"]}")
    testImplementation("io.ktor:ktor-server-test-host:${deps["ktor"]}")

    implementation("com.fasterxml.jackson.core:jackson-databind:${deps["jackson"]}")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:${deps["jackson"]}")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:${deps["jackson"]}")

    implementation("io.jenetics:jpx:1.5.3")

    runtime("ch.qos.logback:logback-classic:1.2.3")
    runtime("org.slf4j:jcl-over-slf4j:${deps["slf4j"]}")
    implementation("org.slf4j:jul-to-slf4j:${deps["slf4j"]}")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

tasks.test {
    useJUnitPlatform()
}

configurations.all {
    // don't let commons logging creep into the classpath; use jcl-over-slf4j instead
    exclude("commons-logging", "commons-logging")
}

// compile to java 8 bytecode, not java 6
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
