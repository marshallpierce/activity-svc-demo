package org.mpierce.activitydemo

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.jenetics.jpx.GPX
import io.jenetics.jpx.geom.Geoid
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.http.content.streamProvider
import io.ktor.jackson.JacksonConverter
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import java.io.ByteArrayInputStream
import java.time.Duration
import java.time.Instant
import kotlin.streams.toList

fun main(args: Array<String>) {
    // Use SLF4J for java.util.logging
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()

    val jackson = configuredObjectMapper()

    val server = embeddedServer(Netty, port = 9080) {
        // any other ktor features would be set up here
        configureJackson(this, jackson)

        routing {
            post("/analyze-gpx") {
                handleUpload(call)
            }
        }
    }
    server.start(wait = true)
}

suspend fun handleUpload(call: ApplicationCall) {
    val part = call.receiveMultipart()
            .readAllParts()
            .filterIsInstance<PartData.FileItem>()
            .firstOrNull { it.name == "file" }
    if (part == null) {
        call.respond(HttpStatusCode.BadRequest)
        return
    }

    val g = withContext(Dispatchers.IO) {
        part.streamProvider()
                // work around ktor 1.2.4 bug: reading one byte at a time is broken, so read them all up front
                // https://github.com/ktorio/ktor/issues/1347
                .use { stream -> GPX.reader().read(ByteArrayInputStream(stream.readAllBytes())) }
    }

    call.respond(
            g.tracks()
                    .map { track ->
                        val spans = track.segments
                                .asSequence()
                                // just glue all the segments together as a starting point
                                .flatMap { it.points.asSequence() }
                                .windowed(2)
                                .map {
                                    Span(Geoid.WGS84.distance(it[0], it[1]).toDouble(),
                                            Duration.between(it[0].time.get(), it[1].time.get()).toMillis())
                                }
                                .toList()

                        TrackStuff(track.name.orElse(""), spans)
                    }
                    .toList()
    )
}

class TrackStuff(
        @JsonProperty("name")
        val name: String,
        @JsonProperty("spans")
        val spans: List<Span>
)

// Between two points
class Span(
        @JsonProperty("distanceMeters")
        val distance: Double,
        @JsonProperty("durationMillis")
        val duration: Long
)


fun configureJackson(app: Application, objectMapper: ObjectMapper) {
    app.install(ContentNegotiation) {
        register(ContentType.Application.Json, JacksonConverter(objectMapper))
    }
}

fun configuredObjectMapper(): ObjectMapper {
    return ObjectMapper().apply {
        // Write dates in ISO8601
        setConfig(serializationConfig.withoutFeatures(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS))
        // Don't barf when deserializing json with extra stuff in it
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        // Kotlin support
        registerModule(KotlinModule())
        // Handle Java 8's new time types
        registerModule(JavaTimeModule())
    }
}

